# Section header
match($0, /^### (.+)/, matches)        { printf "\n\n%s:\n\n", matches[1] }
# Goal with description
match($0, /^(.+):.* ## (.+)/, matches) { printf "  %-30s %s\n", matches[1], matches[2]}
