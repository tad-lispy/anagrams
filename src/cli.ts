import * as readline from "readline"
import * as anagrams from "./anagrams.js"
import index from "./index.js"

const rl = readline.createInterface ({
    input: process.stdin,
    output: process.stdout,
})

console.log ("Index ready. Type!")
rl.prompt ()
rl.on ("line", (line) => {
    console.log (`Looking for anagrams of ${ line}`)
    console.time (line)
    for (const sentence of anagrams.phrases (index, line)) {
        console.log (sentence)
    }
    console.timeEnd (line)
    rl.prompt ()
})
