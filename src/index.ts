import { promises as fs } from "fs"
import type { Index } from "./anagrams"

export default await loadIndex ("data/english-words-lowercase-small.txt")

async function loadIndex (pathname: string): Promise <Index> {
    console.log ("Loading index")
    console.time ("loading index")
    const data = await fs.readFile(pathname, "utf-8")
    const words = data.split("\n")
    const index: Index = new Map()
    for (const word of words) {
        const indexed = index.get(word.length) || new Set ()
        index.set(word.length, indexed.add (word))
    }
    console.timeEnd ("loading index")
    return index
}
