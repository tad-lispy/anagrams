import * as anagrams from "./anagrams.js"
import index from "./index.js"
import express from "express"
import * as stream from "stream"

const app = express ()
app.use (express.text ())
app.post ("/", (req, res) => {
    if (typeof req.body === "string") {
        console.log (`Looking for anagrams of: \n${ req.body }`)
        console.time (req.body)

        stream.Readable
            .from (anagrams.phrases (index, req.body), {
                encoding: "utf-8",
            })
            .pipe (new stream.Transform ({
                transform (chunk, encoding, done) {
                    const phrase = chunk.toString ("utf-8")
                    done (null, phrase + "\n")
                }
            }))
            .pipe (res)
            .on("finish", () => {
                console.timeEnd(req.body)

            })

    } else {
        res
            .status (400)
            .send (`Invalid body. Maybe set "content-type" header to "text/plain"?`)
    }
})

app.listen (8000)
