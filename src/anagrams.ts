export type Index = Map <number, Set <string>>

export default function * anagrams (index: Index, input: string): Generator <string, void, void> {
    const indexed = index.get (input.length) || []
    const candidates = new Set(indexed).add (input)

    for (const candidate of candidates) {
        if (isAnagram (candidate, input)) {
            yield candidate
        }
    }
}

export function * phrases (index: Index, input : string): Generator <string, void, void> {
    const words = input.toLowerCase ().split (/\W+/)

    yield * combinations (index, words, [])
}

export function isAnagram (a: string, b: string): boolean {
    if (a.length !== b.length) return false

    const letters_a = a.split ("").sort ()
    const letters_b = b.split ("").sort ()

    return letters_a.every ((letter, index) => letters_b[index] === letter)
}

function * combinations (index: Index, input: Array <string>, prefix: Array <string>): Generator <string, void, void> {
    const [ word, ...rest ] = input
    if (! word) {
        yield prefix.join (" ")
        return
    }

    for (const anagram of anagrams (index, word)) {
        yield * combinations (index, rest, prefix.concat (anagram))
    }
}
