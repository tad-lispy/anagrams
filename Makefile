include Makefile.d/defaults.mk

all: ## Run unit tests and build the program (DEFAULT)
all: dist
.PHONY: all

node-nix-expressions := node-packages.nix node-dependencies.nix node-env.nix node-supplement.nix

help: ## Print this help message
help: # TODO: Handle section headers in awk script
	@
	echo "Useage: make [ goals ]"
	echo
	echo "Available goals:"
	echo
	cat $(MAKEFILE_LIST) | awk -f Makefile.d/make-goals.awk
.PHONY: help

test:
	time parallel \
		--arg-file data/sample-input.txt \
		--pipepart http POST :8000/ content-type:text/plain --stream
.PHONY: test

run: ## Run examples. Use examples=<glob> to filter.
run: dist
run:
	node .

dist: ## Compile the web extension and other assets to the dist/ directory.
dist: $(shell find src/ -type f)
dist:
	tsc
	touch $@

install: ## Install the program in the ${prefix} directory
install: prefix ?= $(out)
install: dist
	test $(prefix)
	mkdir --parents $(prefix)
	cp --recursive dist/* $(prefix)
.PHONY: install

node_modules: $(node-nix-expressions)
	npm clean-install

package-lock.json: package.json
	npm install --package-lock-only

### DEVELOPMENT

develop: ## Start a development server with hot reloading
develop: version ?= $(development-version)
develop: web-extension/manifest.json
develop:
	$(error "Not implemented")
.PHONY: develop

clean: ## Remove all files set to be ignored by git
clean:
	git clean -dfX
.PHONY: clean


### NIX SPECIFIC

export nix := nix --experimental-features "nix-command flakes"
export nix-cache-name := software-garden

result: ## Build the program using Nix
result:
	cachix use $(nix-cache-name)
	$(nix) build --print-build-logs

nix-cache: ## Push Nix binary cache to Cachix
nix-cache: result
	$(nix) flake archive --json \
	| jq --raw-output '.path, (.inputs | to_entries [] .value.path)' \
	| cachix push $(nix-cache-name)

	$(nix) path-info --recursive \
	| cachix push $(nix-cache-name)

serve-result: ## Serve the program built using Nix
serve-result: result
	miniserve result
.PHONY: serve-result

$(node-nix-expressions): package-lock.json node-supplement.json
	rm -rf node_modules
	node2nix \
		--development \
		--input package.json \
		--lock package-lock.json \
		--composition node-dependencies.nix \
		--supplement-input node-supplement.json \
		--supplement-output node-supplement.nix
