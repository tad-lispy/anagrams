{
  description = "CPU Bound Node.js playground";

  inputs = {
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    flake-utils = {
      url = "github:numtide/flake-utils";
    };
  };

  outputs = { self, nixpkgs, flake-utils, flake-compat }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };

        common-inputs = [
            pkgs.nodejs-16_x
            pkgs.nodePackages.typescript
            pkgs.gnumake
            pkgs.jq
            pkgs.parallel
        ];

      in rec {
        defaultPackage = pkgs.stdenv.mkDerivation {
          buildInputs = common-inputs ++ [
          ];
          name = "cpu-bound";
          src = self;
          buildPhase = "make dist";
          installPhase = "make install";
        };

        devShell = pkgs.mkShell {
          name = "cpu-bound-development-shell";
          src = self;
          buildInputs = common-inputs ++ [
            pkgs.nodePackages.node2nix
            pkgs.nodePackages.typescript-language-server
            pkgs.cachix
          ];
        };
      }
    );
}
